/*
 * FizzBuzz.java
 * FizzBuzzTest
 *
 * Copyright 2013-2014 Insight Plus, Inc. All rights reserved.
 * Written by Hiroyuki Shigeta <shigeta@insight-plus.jp>.
 */
package org.glgl.fizzbuzz;

import java.util.stream.IntStream;

public class FizzBuzz {
    public String convert(int source) {

        if (source % (3 * 5) == 0) {
            return "FizzBuzz";
        } else if (source % 3 == 0) {
            return "Fizz";
        } else if (source % 5 == 0) {
            return "Buzz";
        }

        return String.valueOf(source);
    }

    public static void main(String... args) {
        FizzBuzz fizzBuzz = new FizzBuzz();

        IntStream.range(1, 50)
                .forEach(n -> System.out.println(fizzBuzz.convert(n)));
    }
}
