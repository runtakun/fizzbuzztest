/*
 * FizzBuzzTest.java
 * FizzBuzzTest
 *
 * Copyright 2013-2014 Insight Plus, Inc. All rights reserved.
 * Written by Hiroyuki Shigeta <shigeta@insight-plus.jp>.
 */
package org.glgl.fizzbuzz;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class FizzBuzzTest {

    private FizzBuzz sut;

    @Before
    public void setUp() {
        sut = new FizzBuzz();
    }

    @Test
    public void _1を与えたら1を返す() {
        assertThat(sut.convert(1), is("1"));
    }

    @Test
    public void _2を与えたら2を返す() {
        assertThat(sut.convert(2), is("2"));
    }

    @Test
    public void _3を与えたらFizzを返す() {
        assertThat(sut.convert(3), is("Fizz"));
    }

    @Test
    public void _5を与えたらBuzzを返す() {
        assertThat(sut.convert(5), is("Buzz"));
    }

    @Test
    public void _15を与えたらFizzBuzzを返す() {
        assertThat(sut.convert(15), is("FizzBuzz"));
    }
}
